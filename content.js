// import strings from './strings.js';

// Listen for a message to check if the content script is ready
chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    if (message.action === 'checkContentScriptReady') {
        // Respond with whether the content script is ready
        sendResponse({ isReady: true });
    }
});


chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    const messageAction1 = ['man-peer', 'man-subordinate1', 'sh-peer'];
    const messageAction2 = ['man-subordinate2'];
    const messageAction3 = ['eng-peer', 'eng-manager', 'eng-sectionHead', 'man-manager', 'sh-manager', 'sh-subordinate'];
    // const messageAction1 = [strings.manPeer, strings.manSubordinate1, strings.shPeer];
    // const messageAction2 = [strings.manSubordinate2];
    // const messageAction3 = [strings.engPeer, strings.engManager, strings.engSectionHead, strings.manManager, strings.shManager, strings.shSubordinate];

    console.log("message.action: ", message.action);

    if (messageAction1.includes(message.action)) handleAction1();
    if (messageAction2.includes(message.action)) handleAction2();
    if (messageAction3.includes(message.action)) handleAction3();

});

function handleAction1(){
    
    const rows = document.querySelectorAll('.table tbody tr');
    rows.forEach(function(row) {
        var firstChild = row.children[1];
        var radioButton = firstChild.querySelector('input[type="radio"]');
        if (radioButton)
    {
            radioButton.checked = true;
        }
    });
    
    document.querySelector("button[type='submit']").click();
}

function handleAction2(){

    const tables = document.querySelectorAll('table.table-hover');
    tables.forEach((table) => {
    const thead = table.querySelector('thead');
    const thElements = thead.querySelectorAll('th');
    let positionOf5_0 = -1;

    thElements.forEach((th, index) => {
        if (th.textContent.includes("5.0")) {
        positionOf5_0 = index - 1;
        }
    });

    if (positionOf5_0 !== -1) {
        const tbody = table.querySelector('tbody');
        const tdElements = tbody.querySelectorAll('td');

        if (tdElements.length > positionOf5_0) {
        const radio = tdElements[positionOf5_0].querySelector('input[type="radio"]');
        if (radio) {
            radio.checked = true;
        }
        }
    }
    });

    document.querySelector("button[type='submit']").click();
}

function handleAction3(){

    const rows = document.querySelectorAll('.js_radio');
    rows.forEach(row => {
        row.children[0].children[0].children[0].checked = true;
    });

    document.querySelector("button[type='submit']").click();
}


// Inform the popup that the content script is ready
chrome.runtime.sendMessage({ action: 'contentScriptReady' });

