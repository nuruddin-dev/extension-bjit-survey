import strings from './strings.js';

var logoutButton, header, homeDiv, engineerDiv, managerDiv, sectionHeadDiv;

document.addEventListener('DOMContentLoaded', function () {
    console.log("document loaded");

    // Find the maximum width
    const buttonContainers = document.querySelectorAll('.button-container');
    let maxWidth = 0;

    buttonContainers.forEach(container => {
        const width = container.clientWidth;
        maxWidth = Math.max(maxWidth, width);
    });

    // Set the maximum width to all button-container elements
    buttonContainers.forEach(container => {
        container.style.width = `${maxWidth}px`;
    });

    header = document.getElementById('header');
    homeDiv = document.getElementById('homeBlock');
    engineerDiv = document.getElementById('engineerBlock');
    managerDiv = document.getElementById('managerBlock');
    sectionHeadDiv = document.getElementById('sectionHeadBlock');

    var engPeer = document.getElementById(strings.engPeer);
    var engManager = document.getElementById(strings.engManager);
    var engSectionHead = document.getElementById(strings.engSectionHead);

    var manManager = document.getElementById(strings.manManager);
    var manPeer = document.getElementById(strings.manPeer);
    var manSubordinate1 = document.getElementById(strings.manSubordinate1);
    var manSubordinate2 = document.getElementById(strings.manSubordinate2);

    var shManager = document.getElementById(strings.shManager);
    var shPeer = document.getElementById(strings.shPeer);
    var shSubordinate = document.getElementById(strings.shSubordinate);


    // Add event listener for the "Logout" button click
    logoutButton = document.getElementById('logoutButton');
    if(logoutButton){
        logoutButton.addEventListener('click', function(){
            showSelectedPage("home", homeDiv, engineerDiv, managerDiv, sectionHeadDiv, logoutButton, header);
        })
    }
    
    // Add event listener for the "Continue" button click
    const continueButton = document.getElementById('continueButton');
    if (continueButton) {
        continueButton.addEventListener('click', function(){
            const selectedOption = document.querySelector('input[name="userSelection"]:checked');
    
            const selectionValue = selectedOption.value;
            localStorage.setItem('userSelection', selectionValue);
            showSelectedPage(selectionValue, homeDiv, engineerDiv, managerDiv, sectionHeadDiv, logoutButton, header);
            
        });
    }


    // Check if user already made a selection
    const storedSelection = localStorage.getItem('userSelection');
    showSelectedPage(storedSelection, homeDiv, engineerDiv, managerDiv, sectionHeadDiv, logoutButton, header);

    
    if (engPeer) {
        engPeer.addEventListener('click', function () {
            sendMsg(strings.engPeer);
        });
    }
    
    if (engManager) {
        engManager.addEventListener('click', function () {
            sendMsg(strings.engManager);
        });
    }
    
    if (engSectionHead) {
        engSectionHead.addEventListener('click', function () {
            sendMsg(strings.engSectionHead);
        });
    }
    
    if (manPeer) {
        manPeer.addEventListener('click', function () {
            sendMsg(strings.manPeer);
        });
    }
    
    if (manManager) {
        manManager.addEventListener('click', function () {
            sendMsg(strings.manManager);
        });
    }

    if (manSubordinate1) {
        manSubordinate1.addEventListener('click', function () {
            sendMsg(strings.manSubordinate1);
        });
    }

    if (manSubordinate2) {
        manSubordinate2.addEventListener('click', function () {
            sendMsg(strings.manSubordinate2);
        });
    }
    
    if (shManager) {
        shManager.addEventListener('click', function () {
            sendMsg(strings.shManager);
        });
    }
    
    if (shPeer) {
        shPeer.addEventListener('click', function () {
            sendMsg(strings.shPeer);
        });
    }
    
    if (shSubordinate) {
        shSubordinate.addEventListener('click', function () {
            sendMsg(strings.shSubordinate);
        });
    }
});

function showSelectedPage(storedSelection, homeDiv, engineerDiv, managerDiv, sectionHeadDiv, logoutButton, header){
    switch(storedSelection){
        case 'forEngineer':
            showPage('block', 'Position: Engineer', 'block', 'none', 'none', 'none');
            // logoutButton.style.display = 'block';
            // header.innerText ='Position: Engineer';
            // engineerDiv.style.display = 'block';
            // managerDiv.style.display = 'none';
            // sectionHeadDiv.style.display = 'none';
            // homeDiv.style.display = 'none';
            break;
        case 'forManager':
            logoutButton.style.display = 'block';
            header.innerText ='Position: Manager';
            managerDiv.style.display = 'block';
            engineerDiv.style.display = 'none';
            sectionHeadDiv.style.display = 'none';
            homeDiv.style.display = 'none';
            break;
        case 'forSectionHead':
            logoutButton.style.display = 'block';
            header.innerText ='Position: Section Head';
            sectionHeadDiv.style.display = 'block';
            engineerDiv.style.display = 'none';
            managerDiv.style.display = 'none';
            homeDiv.style.display = 'none';
            break;

        default:
            logoutButton.style.display = 'none';
            header.innerText ='Please select a option:';
            homeDiv.style.display = 'block';
            engineerDiv.style.display = 'none';
            sectionHeadDiv.style.display = 'none';
            managerDiv.style.display = 'none';
    }

}

function showPage(logoutButtonDisplay, headerDisplay, engineerDivDisplay, managerDivDisplay, sectionHeadDivDisplay, homeDivDisplay ){
    logoutButton.style.display = logoutButtonDisplay;
    header.innerText = headerDisplay;
    engineerDiv.style.display = engineerDivDisplay;
    managerDiv.style.display = managerDivDisplay;
    sectionHeadDiv.style.display = sectionHeadDivDisplay;
    homeDiv.style.display = homeDivDisplay;
}

function sendMsg(messageAction){
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        var activeTab = tabs.find(tab => tab.active === true);
        // Send a message to check if content script is ready
        chrome.tabs.sendMessage(activeTab.id, { action: 'checkContentScriptReady' }, function (response) {
            if (response && response.isReady) {
                // Content script is ready, send your actual message here
                chrome.tabs.sendMessage(activeTab.id, { action: messageAction }); 
            } else {
                console.log('Content script is not ready yet.');
            }
        });
    });
}