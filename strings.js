const strings = {
    engPeer: "eng-peer",
    engManager: "eng-manager",
    engSectionHead: "eng-sectionHead",
    manManager: "man-manager",
    manPeer: "man-peer",
    manSubordinate1: "man-subordinate1",
    manSubordinate2: "man-subordinate2",
    shManager: "sh-manager",
    shPeer: "sh-peer",
    shSubordinate: "sh-subordinate"
};

export default strings;